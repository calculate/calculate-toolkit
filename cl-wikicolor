#!/usr/bin/env python
# -*- coding: utf-8 -*-

# Copyright 2017 Mir Calculate. http://www.calculate-linux.org
#
#  Licensed under the Apache License, Version 2.0 (the "License");
#  you may not use this file except in compliance with the License.
#  You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
#  Unless required by applicable law or agreed to in writing, software
#  distributed under the License is distributed on an "AS IS" BASIS,
#  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#  See the License for the specific language governing permissions and
#  limitations under the License.
import argparse
import sys
from calculate.lib.utils.colortext.converter import ConsoleCodes256Converter
from calculate.lib.utils.colortext.output import ColorTerminal256Output


def convert(s):
    """Преобразовать вывод консоли в xml для внутреннего использования"""
    return ConsoleCodes256Converter(output=ColorTerminal256Output(escSymb='♯')).transform(s)

def rev_convert(s):
    """Преобразовать вывод консоли в xml для внутреннего использования"""
    return ConsoleCodes256Converter(output=ColorTerminal256Output(), escSymb='♯').transform(s)

class ArgumentParserCache(argparse.ArgumentParser):
    def __init__(self):
        super(ArgumentParserCache, self).__init__(
            description="Convert terminal color output to calculate wiki")
        self.add_argument('-r', '--reverse', action="store_true",
                          help='reverse conversion')


if __name__ == '__main__':
    apv = ArgumentParserCache()
    args = apv.parse_args()
    if args.reverse:
        sys.stdout.write(rev_convert(sys.stdin.read()))
    else:
        sys.stdout.write(convert(sys.stdin.read()))
