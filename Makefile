# Makefile
INSTALL = install

EXES = all
MAKE = make

SUBDIRS = po

all:

subdirs:
	for dir in $(SUBDIRS); do \
		$(MAKE) -C $$dir install; \
	done

install: all subdirs
	@${INSTALL} -D -m 0755 cl-kernel ${DESTDIR}/usr/sbin/cl-kernel
	@${INSTALL} -D -m 0755 cl-check-templates ${DESTDIR}/usr/sbin/cl-check-templates
	@${INSTALL} -D -m 0755 cl-system-dump ${DESTDIR}/usr/sbin/cl-system-dump
	@${INSTALL} -D -m 0755 cl-wikicolor ${DESTDIR}/usr/bin/cl-wikicolor
	@${INSTALL} -D -m 0755 cl-lxc ${DESTDIR}/usr/sbin/cl-lxc
